# Testing

Build container with testing tools:

```bash
docker build -t testing_jaco_sat - < tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/:/Jaco-Satellite/ \
  -it testing_jaco_sat
```

For syntax tests, check out the steps in the [gitlab-ci](../.gitlab-ci.yml#L69) file.
