FROM docker.io/ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
WORKDIR /

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y --no-install-recommends nano wget curl git
RUN apt-get install -y --no-install-recommends python3-pip

# Some audio dependencies
RUN apt-get install -y --no-install-recommends alsa-base alsa-utils
RUN apt-get install -y --no-install-recommends libsndfile1

# Update pip
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir --upgrade setuptools

# Preinstall numpy
RUN pip3 install --no-cache-dir --upgrade numpy

# Install jacolib
RUN mkdir /Jaco-Satellite/
RUN cd /Jaco-Satellite/; git clone https://gitlab.com/Jaco-Assistant/jacolib.git
RUN pip3 install --no-cache-dir --upgrade --user -e /Jaco-Satellite/jacolib

# Test jacolib installation
RUN cd /tmp/; python3 -c "import jacolib;"
RUN pip3 install --no-cache-dir --upgrade pytest pytest-cov
RUN cd /Jaco-Satellite/jacolib/; pytest --cov=jacolib
RUN pip3 uninstall -y pytest pytest-cov

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /Jaco-Satellite/
CMD ["/bin/bash"]
