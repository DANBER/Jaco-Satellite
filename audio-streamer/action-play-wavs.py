import base64
import os
import subprocess
import time
from typing import Any

from paho.mqtt.client import Client, MQTTMessage

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

input_topic = "Jaco/{}/AudioWav"
output_topic = "Jaco/StreamWav/Status"

global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
input_topic = input_topic.format(satellite_name)
architecture = utils.load_architecture()
alsa_device = global_config["speaker_device_name"]

# ==================================================================================================


def on_connect(client: Client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(
    client: Client,  # pylint: disable=unused-argument
    userdata: Any,  # pylint: disable=unused-argument
    msg: MQTTMessage,
):
    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        wav_content = payload["data"].encode()
        wav_content = base64.b64decode(wav_content)

        # Use differnt names for the audio types, that they don't overwrite each other
        tmp_audio_path = "/tmp/sound_{}.wav".format(payload["type"])

        with open(tmp_audio_path, "wb") as file:
            file.write(wav_content)

        start_message = {
            "playing": True,
            "satellite": satellite_name,
            "timestamp": time.time(),
            "type": payload["type"],
        }
        msg_out = comm_tools.encrypt_msg(start_message, output_topic)
        client.publish(output_topic, msg_out)
        client.loop_write()
        print("Starting playing sound:", start_message)

        cmd = "aplay -Dplug:{} {}"
        cmd = cmd.format(alsa_device, tmp_audio_path)
        with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
            sp.wait()

        cmd = "rm {}".format(tmp_audio_path)
        with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
            sp.wait()

        finish_message = {
            "playing": False,
            "satellite": satellite_name,
            "timestamp": time.time(),
            "type": payload["type"],
        }
        msg_out = comm_tools.encrypt_msg(finish_message, output_topic)
        client.publish(output_topic, msg_out)
        client.loop_write()
        print("Finished playing sound:", finish_message)


# ==================================================================================================


def main():
    client = comm_tools.connect_mqtt_client(global_config, on_connect, on_message)
    client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
