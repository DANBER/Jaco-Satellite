# Audio Streamer Module

This readme describes how to setup and debug the Audio Streamer Module.
It is used to stream microphone inputs and play speech outputs.

All commands are run from `Jaco-Satellite` directory.

## Setup

To setup and test your speaker and microphone there are several steps to execute.
Most of the steps are run from inside the container, that you don't have to install additional programs.

- If your `~/.asoundrc` file is empty, copy the contents of one of the [asound-templates](../userdata/) from the `userdata` directory into it.
  This should work, with slight adjustments, for most audio setups.

- Start the container (adjust the architecture if needed):

  ```bash
  export ARCH=amd64
  docker run --network host --rm --ipc host --device /dev/snd \
    --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
    --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
    --volume `pwd`/jacolib/:/Jaco-Satellite/jacolib/:ro \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    -it audio_streamer_${ARCH}
  ```

- Setup your speakers:

  - Find out the card-id and device-id of your speaker:

    ```bash
    # Run in container
    aplay --list-devices
    ```

  - Test audio:

    ```bash
    # The first number in "hw:2,0" is the card-id, the second one the device-id
    speaker-test -t wav --channels 2 --device="hw:2,0"
    ```

  - Update the `~/.asoundrc` file with your device values. Update it from outside of the container and restart the container after a change. \
    (To apply the update directly to your computer, restart alsa with `sudo /etc/init.d/alsa-utils restart` or log out and in again)

  - Test your speaker again:

    ```bash
    # Run in container
    speaker-test -t wav --channels 2

    # You also can use device names like this
    speaker-test -t wav --channels 1 --device="default"
    ```

  - If the card id is correct and you still don't hear anything, try to run the command outside the container, directly on the host.

  - If your running this on a normal computer, which normally uses _PulseAudio_ as default service, you might, in some cases, need to adjust `/usr/share/alsa/alsa.conf.d/pulse.conf` that it includes `~/.asoundrc` instead of `/usr/share/alsa/pulse-alsa.conf` (see [link](https://bugs.launchpad.net/ubuntu/+source/pulseaudio/+bug/295832)).

  - For further information checkout this [link](https://www.alsa-project.org/main/index.php/Asoundrc#The_naming_of_PCM_devices), this troubleshooting [page](https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture/Troubleshooting),
    or try to google setups for your specific hardware devices.

- Test your microphone:

  - First, get some microphone infos. Like before, you need the card-id and the device-id.

    ```bash
    # Run in container
    arecord --list-devices
    ```

  - Update the `~/.asoundrc` file again.
    (The _template_ creates a virtual microphone device which will automatically convert the audio input to the required format. You can find a detailed explanation of all possible options [here](https://www.alsa-project.org/alsa-doc/alsa-lib/pcm_plugins.html#pcm_plugins_dmix) and [here](https://alsa.opensrc.org/ALSA_plugins).)

  - Record 3s of audio and listen to it afterwards:

    ```bash
    # Run in container
    arecord --rate 16000 --format S16_LE --channels=1 --duration 3 --device="default" test-mic.wav
    aplay test-mic.wav
    ```

  - If it did't work, try to record something using your hardware device directly:

    - First, get infos about sample-rate and channels:

      ```bash
      # Run on host

      # In case pulseaudio is installed run
      pacmd list-sources | grep -Ei "name:|sample spec"

      # Else, for USB-microphones (update the card number with yours)
      cat /proc/asound/card2/stream0

      # Or just try some common sample-rates (48000, 44100, 24000, 22050, 16000, 11025, 8000),
      # together with different channel numbers in the next step
      ```

    - Then try to record something again. Adjust the rate and channels to your device.

      ```bash
      # Run in container
      arecord --rate 44100 --channels=2 --duration 3 --device="hw:1,0" test-mic.wav
      aplay test-mic.wav
      ```

  - If you don't want to use/change the `default` device, update the `global_config.yaml` file with a different device name.

- Test Jaco's connection:

  - Check audio recoding with python, and the microphone volume:

    ```bash
    # Run in container
    python3 /Jaco-Satellite/audio-streamer/action-stream-mic.py --tofile test-mic.wav --duration 7
    aplay test-mic.wav
    ```

    When you're speaking, you should see more than a handful of the volume bars,
    else you have to make your microphone louder in the system settings.
    If you're not speaking you shouldn't see more than about three bars,
    else you might get problems with the detection of the end of your speech input.
    There's also a flag in the config file to increase or lower your microphone volume
    if you can't change it via the system settings or the volume is still to low.

  - The last thing you have have to test now is playing audio over mqtt:

    ```bash
    # Start the mqtt broker (run this in your Jaco-Master directory)
    export ARCH=amd64
    docker run --network host --rm \
      --volume `pwd`/mqtt-broker/:/Jaco-Master/mqtt-broker/:ro \
      --volume `pwd`/jacolib/:/Jaco-Satellite/jacolib/:ro \
      -it mqtt_broker_${ARCH} /bin/bash -c "mosquitto -c /Jaco-Master/mqtt-broker/mosquitto.conf"

    # In a new console window start the audio player action
    export ARCH=amd64
    docker run --network host --rm --ipc host --device /dev/snd \
      --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
      --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
      --volume `pwd`/jacolib/:/Jaco-Satellite/jacolib/:ro \
      --volume ~/.asoundrc:/etc/asound.conf:ro \
      -it audio_streamer_${ARCH} python3 /Jaco-Satellite/audio-streamer/action-play-wavs.py

    # Send an audio file from a third console window
    export ARCH=amd64
    docker run --network host --rm --ipc host --device /dev/snd \
      --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
      --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
      --volume `pwd`/jacolib/:/Jaco-Satellite/jacolib/:ro \
      --volume ~/.asoundrc:/etc/asound.conf:ro \
      -it audio_streamer_${ARCH} python3 /Jaco-Satellite/audio-streamer/tests/test_play_wav.py
    ```

- If you're running this on a raspberry pi you now have to copy the `.asoundrc` file to the root user, because docker-compose loads it from there.

  ```bash
  sudo cp ~/.asoundrc /root/.asoundrc
  ```

<br>

## Debugging

Build container:

```bash
docker build -t audio_streamer_amd64 - < audio-streamer/Containerfile_amd64
docker buildx build --platform=linux/arm64 -t audio_streamer_arm64 - < audio-streamer/Containerfile_arm64
docker buildx build --platform=linux/arm -t audio_streamer_armhf - < audio-streamer/Containerfile_armhf
```

Test speakers with python:

```bash
# Run in container
# You should hear a steady sound for 10s and then the program ends itself

python3 /Jaco-Satellite/audio-streamer/tests/test_speaker_python.py
```

Test microphone streaming: \
(Assumes stream-mic-action and mqtt-broker already running)

```bash
# Run in container
# You should hear an echo of your microphone input
python3 /Jaco-Satellite/audio-streamer/tests/test_mic_stream.py
```
