import argparse
import base64
import os
import time
import wave

import alsaaudio
import numpy as np
from paho.mqtt.client import Client

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

sample_rate = 16000
output_topic = "Jaco/{}/AudioStream"

global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
output_topic = output_topic.format(satellite_name)
device = global_config["microphone_device_name"]

buffer_duration = 0.030
used_buffer_size = int(sample_rate * buffer_duration)
frame_buffer = np.array([], dtype=np.int16)

mqtt_client: Client
audio_stream: alsaaudio.PCM
audio_write_array = np.array([], dtype=np.int16)

# ==================================================================================================


def print_max_volume(frames: np.ndarray) -> None:
    volume = max(0, (np.max(frames) / np.iinfo(frames.dtype).max))
    volume = int(volume * 70)
    print(" " * 80, end="\r")
    print("Volume:", "|" * volume, end="\r")


# ==================================================================================================


def apply_volume_factor(frames: np.ndarray, factor: float) -> np.ndarray:
    """Apply factor to sound volume and clip values to fit in datatype range"""

    if factor == 1.0:
        return frames

    frames = frames.astype(np.float32)
    frames = frames * factor
    frames = np.clip(frames, np.iinfo(np.int16).min, np.iinfo(np.int16).max)
    frames = frames.astype(np.int16)

    return frames


# ==================================================================================================


def preprocess_audio(frames: np.ndarray) -> np.ndarray:
    frames = apply_volume_factor(frames, global_config["volume_multiplicator"])
    print_max_volume(frames)
    return frames


# ==================================================================================================


def publish_audio_frame(frames: np.ndarray) -> None:

    sframes = base64.b64encode(frames.tobytes()).decode()
    payload = {
        "data": sframes,
        "timestamp": time.time(),
    }
    msg = comm_tools.encrypt_msg(payload, output_topic)
    mqtt_client.publish(output_topic, msg)


# ==================================================================================================


def main() -> None:
    global mqtt_client, audio_stream, frame_buffer, audio_write_array

    parser = argparse.ArgumentParser(description="Stream file.wav to topic")
    parser.add_argument("--tofile", default="", help="Path of the file.wav to write to")
    parser.add_argument("--duration", default=3, type=int, help="Recording duration")
    args = parser.parse_args()

    audio_stream = alsaaudio.PCM(
        alsaaudio.PCM_CAPTURE,
        alsaaudio.PCM_NONBLOCK,
        channels=1,
        rate=sample_rate,
        format=alsaaudio.PCM_FORMAT_S16_LE,
        device=device,
    )

    if args.tofile == "":
        mqtt_client = comm_tools.connect_mqtt_client(global_config, None, None)
        print("Started streaming audio")
    else:
        print("Started recording audio")

    time_start = time.time()
    while args.tofile == "" or (time.time() - time_start) < args.duration:
        n, data = audio_stream.read()

        if n <= 0:
            time.sleep(0.001)
            continue

        frames = np.frombuffer(data, dtype="int16")
        frame_buffer = np.append(frame_buffer, frames)

        while len(frame_buffer) >= used_buffer_size:
            bframes = np.array(frame_buffer[:used_buffer_size], dtype=np.int16)
            frame_buffer = frame_buffer[used_buffer_size:]
            bframes = preprocess_audio(bframes)

            if args.tofile == "":
                publish_audio_frame(bframes)
                mqtt_client.loop()
            else:
                audio_write_array = np.append(audio_write_array, bframes)

    if args.tofile != "":
        wf = wave.open(args.tofile, "wb")
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(16000)
        wf.writeframes(audio_write_array.tobytes())
        wf.close()


# ==================================================================================================

if __name__ == "__main__":
    main()
