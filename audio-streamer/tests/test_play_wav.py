import argparse
import base64
import os
import time

from jacolib import comm_tools, utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../../")

output_topic = "Jaco/{}/AudioWav"
global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
output_topic = output_topic.format(satellite_name)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Stream file.wav to topic")
    parser.add_argument(
        "--path_wav",
        default=filepath + "asr_started.wav",
        help="Path of the file.wav to test",
    )
    args = parser.parse_args()

    client = comm_tools.connect_mqtt_client(global_config, None, None)

    with open(args.path_wav, "rb") as file:
        content = file.read()

    content = base64.b64encode(content).decode()
    payload = {
        "data": content,
        "timestamp": time.time(),
        "type": "input_status",
    }
    msg = comm_tools.encrypt_msg(payload, output_topic)
    client.publish(output_topic, msg)
    client.loop()
    client.disconnect()


# ==================================================================================================

if __name__ == "__main__":
    main()
