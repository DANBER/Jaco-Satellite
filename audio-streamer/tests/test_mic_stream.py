import argparse
import base64
import os

import alsaaudio
import numpy as np

from jacolib import comm_tools, utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../../")

sample_rate_input = 16000
input_topic = "Jaco/{}/AudioStream"
streamer: alsaaudio.PCM

global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
input_topic = input_topic.format(satellite_name)

# ==================================================================================================


def print_max_volume(frames: np.ndarray) -> None:
    volume = max(0, (np.max(frames) / np.iinfo(frames.dtype).max))
    volume = int(volume * 70)
    print(" " * 80, end="\r")
    print("Volume:", "|" * volume, end="\r")


# ==================================================================================================


def on_connect(client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        frames = payload["data"].encode()
        frames = base64.b64decode(frames)
        frames = np.frombuffer(frames, dtype=np.int16)

        print_max_volume(frames)
        frames = frames.tobytes()
        streamer.write(frames)


# ==================================================================================================


def main():
    global streamer

    parser = argparse.ArgumentParser(description="Stream audio from mqtt topic")
    parser.add_argument(
        "--device_name",
        default="default",
        type=str,
        help="ALSA device name of the speakers",
    )
    args = parser.parse_args()

    client = comm_tools.connect_mqtt_client(global_config, on_connect, on_message)

    streamer = alsaaudio.PCM(
        alsaaudio.PCM_PLAYBACK,
        channels=1,
        rate=sample_rate_input,
        format=alsaaudio.PCM_FORMAT_S16_LE,
        periodsize=160,
        device=args.device_name,
    )

    print("Started listening to audio stream")
    client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
