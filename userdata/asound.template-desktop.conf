# ALSA config file for a simple speaker+microphone setup
# ======================================================

# Use as default device
# This is optional, you can also use 'playback' and/or 'capture' directly as devices in Jaco's config,
# in case you don't want to change your default setup.
pcm.!default {
    type asym
    playback.pcm "playback"
    capture.pcm "capture"
}

# Speaker config
pcm.myspeaker {
    type hw
    card 2  # <-- adjust to your device
    device 0  # <-- adjust to your device
}

pcm.dmixer {
    type dmix  # mixing parallel audio inputs to one output
    slave.pcm "myspeaker"
    ipc_perm 0666  # allow usage by other clients
    ipc_key 111111  # needs to be different for every plugin
    ipc_key_add_uid false  # the plugins in docker need to have the same keys as the host
}

pcm.playback {
    type plug  # automatic sample-rate and channel conversion
    slave.pcm "dmixer"
}

# Microphone config
pcm.mymicro {
    type hw
    card 1  # <-- adjust to your device
    device 0  # <-- adjust to your device
}

pcm.dsnooper {
    type dsnoop  # enables parallel access to the microphone
    slave.pcm "mymicro"
    ipc_perm 0666
    ipc_key 666666
    ipc_key_add_uid false
}

pcm.capture {
    type plug  # automatic sample-rate and channel conversion
    slave.pcm "dsnooper"
    route_policy "average"  # mono input (use average of all channels)
}
