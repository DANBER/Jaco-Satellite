import base64
import os
import time
from typing import Any

import numpy as np
import pvporcupine
from paho.mqtt.client import Client, MQTTMessage

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

output_topic = "Jaco/WakeWord"
input_topic = "Jaco/{}/AudioStream"
global_config = utils.load_global_config()

satellite_name = global_config["unique_satellite_name"].capitalize()
input_topic = input_topic.format(satellite_name)

mqtt_client: Client
porcupine = None
frame_buffer = np.array([], dtype=np.int16)
used_buffer_size = 512


# ==================================================================================================


def load_keyword_listener():
    keyword = global_config["hotword"]
    possible_path = file_path + "custom-wakeword/" + keyword
    sensitivity = [global_config["hotword_sensitivity"]]

    # Check if the user has set a custom hotword
    if os.path.exists(possible_path):
        keywords = [possible_path]
        pcpn = pvporcupine.create(keyword_paths=keywords, sensitivities=sensitivity)
    else:
        keywords = [keyword]
        pcpn = pvporcupine.create(keywords=keywords, sensitivities=sensitivity)

    return pcpn


# ==================================================================================================


def on_connect(client: Client) -> None:
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(
    client: Client,  # pylint: disable=unused-argument
    userdata: Any,  # pylint: disable=unused-argument
    msg: MQTTMessage,
) -> None:
    global frame_buffer

    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        frames = payload["data"].encode()
        frames = base64.b64decode(frames)
        frames = np.frombuffer(frames, dtype=np.int16)
        frame_buffer = np.append(frame_buffer, frames)


# ==================================================================================================


def on_keyword_detection() -> None:
    payload = {
        "data": True,
        "satellite": satellite_name,
        "timestamp": time.time(),
    }
    msg = comm_tools.encrypt_msg(payload, output_topic)
    mqtt_client.publish(output_topic, msg)
    print("Detected a wake word:", payload)


# ==================================================================================================


def main() -> None:
    global mqtt_client, porcupine, frame_buffer

    mqtt_client = comm_tools.connect_mqtt_client(global_config, on_connect, on_message)
    porcupine = load_keyword_listener()
    print("Started listening for keyword")

    while True:
        if len(frame_buffer) >= used_buffer_size:
            frame = np.array(frame_buffer[:used_buffer_size], dtype=np.int16)
            frame_buffer = frame_buffer[used_buffer_size:]

            keyword_index = porcupine.process(frame)
            if keyword_index >= 0:
                on_keyword_detection()

        mqtt_client.loop(timeout=0.01)


# ==================================================================================================

if __name__ == "__main__":
    main()
